<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;

class AuthorController extends Controller
{
    public function index()
    {
        $data = Author::all();
        return view('backend.author.index', compact('data'));
    }

    public function create()
    {
        return view('backend.author.create');
    }

    public function edit($id)
    {
        $data = Author::findOrFail($id);
        return view('backend.author.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Author::create($request->except('image', '_token'));
        return redirect()->action('AuthorController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category = Author::where('id', $id)->firstOrFail();
        $category->update($request->except('image', '_token', '_method'));
        return redirect()->action('AuthorController@index')->with('success', 'Успешно изменено');
    }
}
