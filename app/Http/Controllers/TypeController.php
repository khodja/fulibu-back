<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;

class TypeController extends Controller
{
    public function index()
    {
        $data = Type::all();
        return view('backend.type.index', compact('data'));
    }

    public function create()
    {
        return view('backend.type.create');
    }

    public function edit($id)
    {
        $data = Type::findOrFail($id);
        return view('backend.type.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Type::create($request->except('image', '_token'));
        return redirect()->action('TypeController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category = Type::where('id', $id)->firstOrFail();
        $category->update($request->except('image', '_token', '_method'));
        return redirect()->action('TypeController@index')->with('success', 'Успешно изменено');
    }
}
