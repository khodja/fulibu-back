<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Book;
use App\Traits\ImageHandler;

class SliderController extends Controller
{
    use ImageHandler;

    public function get()
    {
        $data = Slider::all();
        return response()->json(['statusCode' => 200, 'data' => $data], 200);
    }
    public function index()
    {
        $data = Slider::all();
        return view('backend.slider.index', compact('data'));
    }

    public function create()
    {
        $vendor = Book::all();
        return view('backend.slider.create', compact('vendor'));
    }

    public function edit($id)
    {
        $data = Slider::find($id);
        $vendor = Book::all();
        $directory = "uploads/slider/" . $id;
        $images = \File::glob($directory . "/*");
        return view('backend.slider.edit', compact('data', 'id', 'vendor', 'images'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
            'name_en' => 'required',
            'image' => 'required'
        ]);
        $slider = Slider::create([
            'name_ru' => $request->name_ru,
            'name_uz' => $request->name_uz,
            'name_en' => $request->name_en,
            'book_id' => $request->book_id > 0 ? $request->book_id : null,
        ]);
        $this->createFolder('slider', $slider->id);
        $images = $request->file('image');
        $this->storeImages($images, 'slider', $slider->id, 680, 540,'png');
        return redirect()->action('SliderController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
            'name_en' => 'required',
        ]);
        $slider = Slider::findOrFail($id);
        $slider->update($request->except('image', 'method'));
        if ($request->file('image')) {
            $this->createFolder('slider', $slider->id);
            $this->cleanDirectory($slider->id);
            $images = $request->file('image');
            $this->storeImages($images, 'slider', $slider->id, 680, 540,'png');
        }
        return redirect()->action('SliderController@index')->with('success', 'Изменения успешно внесены');
    }

    public function delete($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return redirect()->action('SliderController@index')->with('success', 'Успешно удален');
    }

    public function cleanDirectory($sliderId)
    {
        $pathToDestroy = public_path('uploads/slider/' . $sliderId . '/');
        \File::cleanDirectory($pathToDestroy);
        return 1;
    }

    public function removeImage($sliderId)
    {
        $file_name = $sliderId . '.png';
        $pathToDestroy = public_path('uploads/slider/' . $sliderId . '/' . $file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }
}
