<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phone;
use App\User;
use App\Role;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $user = \Auth::user();
        if ($user->id === auth()->user()->getAuthIdentifier()) {
            $user->update([
                'name' => $request->name,
            ]);
            return response()->json(['statusCode' => 200], 200);
        }
        abort(404);
    }

    public function auth(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'verify_code' => 'required',
        ]);
        $checkForExist = User::where('phone', request('phone'))->first();
        Phone::where([['phone', request('phone')], ['code', request('verify_code')]])->firstOrFail();
        $getLinkedUiid = $request->linked;
        $found_user_id = 1;
        if ($getLinkedUiid) {
            $found_user = User::where('uuid', $getLinkedUiid)->first();
            if ($found_user) {
                $found_user_id = $found_user->id;
            }
        }
        if (!$checkForExist) {
            $user = User::create([
                'phone' => request('phone'),
                'password' => bcrypt(request('verify_code')),
                'uuid' => Str::uuid()->toString(),
                'user_id' => $found_user_id
            ]);
            $role = Role::where('type', 'member')->first();
            $user->roles()->attach($role);
        } else {
            $user = $checkForExist;
        }
        $token = $user->createToken($user->phone);
        $data = [
            'token' => $token->accessToken,
            'registered' => $user->name ? true : false
        ];
        return response()->json(['statusCode' => 200, 'data' => $data], 200);
    }

    public function login()
    {
        $user = User::where(['phone' => request('phone')])->firstOrFail();
        if (\Hash::check(request('password'), $user->password)) {
            $token = $user->createToken($user->phone);
        } else {
            $response = "Password missmatch";
            return response($response, 422);
        }
        return response()->json($token, 200);
    }

    public function setToken(Request $request)
    {
        $request->validate([
            'token' => 'required'
        ]);
        $user = Auth::user();
        $user->update([
            'notification_token' => $request->token
        ]);
        return response()->json('success', 200);
    }

    public function verification(Request $request)
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $password = rand(1000, 9999);
        $success = $this->send_code($request->phone, $password);
        Phone::updateOrCreate(['phone' => $request->phone], [
            'phone' => $request->phone,
            'code' => $password,
        ]);
        if (!$success) {
            return response('sms_not_working', 404);
        }
        return response()->json(['statusCode' => 200], 200);
    }

    public function send_code($phone, $password)
    {
        $verify_password = $password;
        $client = new Client();
        $client->post('http://91.204.239.44/broker-api/send', [
            'auth' => ['fulibuuz', 'iSc458kFV6'],

            'json' => [
                "messages" => [
                    [
                        "recipient" => $phone,
                        "message-id" => $phone . strval(time()),

                        "sms" => [
                            "originator" => "3700",
                            "content" => ["text" => "Ваш код подтверждения: " . $verify_password . '. Наберите его в поле ввода.']
                        ]
                    ]
                ]
            ]
        ]);
        return true;
    }

    public function getUser()
    {
        $user = Auth::user();
        return User::where('id', $user->id)->firstOrFail();
    }

    public function logout()
    {
        request()->user()->token()->revoke();
        return response()->json(['statusCode' => 200], 200);
    }
}
