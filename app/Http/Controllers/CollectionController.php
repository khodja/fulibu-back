<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Collection;
use App\Category;
use App\Author;
use App\Book;

class CollectionController extends Controller
{

    private $validation = [
        'name' => 'required',
        'description' => 'required',
        'rating' => 'required',
        'publish_date' => 'required',
    ];

    public function get()
    {
        $collection = Collection::take(20)->with('books.author')->orderBy('created_at')->get();
        $newest = Book::orderBy('publish_date', 'DESC')->with('types', 'author')->take(20)->get();
        $recommend = Book::where('recommend', true)->with('types', 'author')->take(20)->get();
        $hot = Book::withCount('orders')->with('types', 'author')->orderBy('orders_count')->take(20)->get();
        return response()->json([
            'statusCode' => 200,
            'recommend' => $recommend,
            'hot' => $hot,
            'newest' => $newest,
            'collection' => $collection
        ], 200);
    }

    public function show($id)
    {
        $collection = Collection::where('id', $id)->with('books')->get();
        return response()->json([
            'statusCode' => 200,
            'data' => $collection
        ], 200);
    }

    public function index()
    {
        $data = Collection::paginate(20);
        return view('backend.collection.index', compact('data'));
    }

    public function create()
    {
        $books = Book::all();
        $authors = Author::all();
        return view('backend.collection.create', compact('authors', 'books'));
    }

    public function edit($id)
    {
        $data = Collection::findOrFail($id);
        $books = Book::all();
        return view('backend.collection.edit', compact('data', 'books'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        $sizes = Book::whereIn('id', $request->book_id)->pluck('size');
        $size = 0;
        foreach ($sizes as $size) {
            $size += $size * 1;
        }
        $request['size'] = $size;
        $book = Collection::create($request->except('_token', 'image', 'book_id', 'jfiler-items-exclude-image-0'));
        $book->books()->attach($request->book_id);
        return redirect()->action('CollectionController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        $collection = Collection::where('id', $id)->firstOrFail();
        $collection->update($request->except('_token', 'image', 'book_id', '_method',
            'jfiler-items-exclude-image-0'));
        if ($request->book_id) {
            $collection->books()->sync($request->book_id);
            $sizes = $collection->books()->pluck('size');
            $size = 0;
            foreach ($sizes as $size) {
                $size += $size * 1;
            }
            $collection->update(['size' => $size]);
        }

        return redirect()->action('CollectionController@index')->with('success', 'Успешно изменено');
    }

    public function search()
    {
        $book = Book::with('author');
        $category = request('category_id');
        $keyword = request('keyword');
        $type = request('type');
        $publish_date = request('publish_date');
        $sortBy = request('sort_by');
        $orderBy = request('order_by') === 'ASC' ? 'ASC' : 'DESC';
        if ($keyword) {
            $book = $book->where('name', 'like', '%' . $keyword . '%');
        }
        if ($publish_date) {
            $book = $book->whereDate('publish_date', '>=', Carbon::parse($publish_date));
        }
        if ($type) {
            $book = $book->whereHas('types', function ($query) use ($type) {
                $query->where('type_id', $type);
            });;
        }
        if ($sortBy && $orderBy) {
            $book = $book->orderBy($sortBy, $orderBy);
        }
        if ($category) {
            $book->whereHas('categories', function ($query) use ($category) {
                $query->where('category_id', $category);
            });
        }
        $book = $book->paginate(20);
        return $book;
    }
}
