<?php

namespace App\Http\Controllers;

use App\Book;
use App\Library;
use Illuminate\Http\Request;
use Auth;

class LibraryController extends Controller
{
    public function get()
    {
        return response()->json(['statusCode' => 200, 'data' => Auth::user()->library()->get()], 200);
    }

    public function toggle($id)
    {
        $book_id = $id;
        $user = Auth::user();
        $getBook = $user->library()->where('book_id', $book_id)->first();
        if ($getBook) {
            $getBook->delete();
        } else {
            Book::create([
                'user_id' => $user->id,
                'book_id' => $book_id * 1
            ]);
        }
        return response()->json(['statusCode' => 200], 200);
    }
}
