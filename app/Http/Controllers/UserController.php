<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{

    public function index()
    {
        $data = User::with(['orders' => function ($query) {
            $query->where('is_paid', true);
        }])->where('id', '!=', 1)->paginate(15);
        return view('backend.user.index', compact('data'));
    }

    public function links($id)
    {
        $data = User::where('user_id', $id)->with('orders')->paginate(15);
        return view('backend.user.index', compact('data'));
    }

    public function register(Request $request)
    {
        $user = Auth::user();
        $user->update($request->except('image', 'banner', '_method'));
        return response()->json(['statusCode' => 200, 'data' => $user], 200);
    }
}
