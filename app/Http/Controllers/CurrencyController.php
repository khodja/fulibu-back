<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Currency;

class CurrencyController extends Controller
{
    public function get()
    {
        $data = Currency::get();
        return view('backend.currency.index', compact('data'));
    }

    public function getCurrency()
    {
        $data = Currency::first();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function update()
    {
        $client = new Client();
        $res = $client->request('GET', 'http://cbu.uz/ru/arkhiv-kursov-valyut/json/');

        if ($res->getStatusCode() == 200) {
            $currencies = $res->getBody()->getContents();
            $currencies_decoded = json_decode($currencies, true);
            $getFirst = Currency::first();
            if (!$getFirst) {
                Currency::create(['value' => (float)$currencies_decoded[0]['Rate']]);
            } else {
                $getFirst->update(['value' => (float)$currencies_decoded[0]['Rate']]);
            }
            return redirect()->back()->with('success', 'Успешно');
        } else {
            return redirect()->back()->with('error', 'Ошибка');
        }
    }
}
