<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function get()
    {
        $data = Category::all();
        return response(['statusCode' => 200, 'data' => $data], 200);
    }

    public function index()
    {
        $data = Category::all();
        return view('backend.category.index', compact('data'));
    }

    public function create()
    {
        return view('backend.category.create');
    }

    public function edit($id)
    {
        $data = Category::findOrFail($id);
        return view('backend.category.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);

        Category::create($request->except('image', '_token'));
        return redirect()->action('CategoryController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        $category = Category::where('id', $id)->firstOrFail();
        $category->update($request->except('image', '_token', '_method'));
        return redirect()->action('CategoryController@index')->with('success', 'Успешно изменено');
    }
}
