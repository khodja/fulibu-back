<?php

namespace App\Http\Controllers;

use App\Book;
use App\Currency;
use App\Order;
use App\User;
use GuzzleHttp\Client;
use Auth;

class OrderController extends Controller
{
    private $cash_id = '5d0317fc4f1f4905a829fa5e647de1b7';

    public function checkout()
    {
        \request()->validate([
            'item_id' => 'required'
        ]);
        $findBook = Book::findOrFail(request('item_id'));
        $user = Auth::user();
        $check_exist = $user->orders()->where('item_id', $findBook->id)->first();
        if (!$check_exist) {
            $check_exist = Order::create([
                'user_id' => $user->id,
                'item_id' => $findBook->id,
                'price' => $findBook->price,
                'is_paid' => false,
            ]);
        } else {
            if ($check_exist->is_paid) return response(['statusCode' => 200, 'message' => 'Already Bought!'], 200);
            else {
                $check_exist->update([
                    'price' => $findBook->price
                ]);
            }
        }
        $url = $this->createPaymentUrl($check_exist);
        return response(['statusCode' => 200, 'url' => $url], 200);
    }

    public function handleCheckout()
    {
        $data = request()->json()->all();
        if (!count($data)) {
            return response()->json(['status' => false], 404);
        }
        $user = User::findOrFail($data['userId']);
        $order = Order::where('id', $data['orderId'])->where('user_id', $user->id)->firstOrFail();
        $order->update(['is_paid' => true]);
        $this->updatePyramid($user);
        return response()->json(['status' => true], 200);
    }

    public function updatePyramid($user)
    {
        if ($user->user_id != 1 && !$user->orders()->where('is_paid', true)->count()) {
            $get_user_parent = $user->parent()->first();
            $children = $get_user_parent->children;
            $inner = $children->map(function ($item) {
                $filtered = $item->orders()->where('is_paid', true)->get();
                if ($filtered->count()) {
                    return $filtered->first();
                }
                return null;
            });
            $inner = $inner->filter(function ($item) {
                return $item != null;
            });
            if ($inner->count() >= 4) {
                while ($children->count()) {
                    $child = $children->shift();
                    if ($child->orders()->where('is_paid', true)->count()) {
                        $children = $children->merge($child->children);
                    } else {
                        $user->update(['user_id' => $child->id]);
                        break;
                    }
                }
            }
        }
    }

    private function createPaymentUrl($order)
    {
        $dollar = Currency::first()->value;
        $cash = 'cash=' . $this->cash_id;
        $amount = 'amount=' . $order->price * $dollar * 100;
        $transaction_id = 'transactionId=' . $order->id;
        $order_id = 'orderId=' . $order->id;;
        $user_id = 'userId=' . $order->user_id;
        $redirect_url = 'redirectUrl=' . $this->url_encode("https://fulibu.uz/user");
        $description = 'description=' . "Оплата за покупку книги с fulibu.uz";
        return 'https://oplata.kapitalbank.uz?'
            . $cash . '&' . $redirect_url . '&' . $description . '&'
            . $amount . '&' . $transaction_id . '&' . $order_id . '&' . $user_id;
    }

    private function url_encode($string)
    {
        return urlencode(utf8_encode($string));
    }

    public function get()
    {
        $data = Auth::user()->orders()->with('book')->where('is_paid', 1)->get();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function cashback()
    {
        request()->validate([
            'card' => 'required',
        ]);
        $card = request('card');
        $user = Auth::user();
        $level_cashback = $user->getFriendsAttribute(); /// [0] level [1] cashback
        $current_level = $level_cashback['level'];
        if ($current_level == 6 || $current_level == 0) {
            abort(404);
        }
        $current_cashback = $level_cashback['reward'];
        if ($current_cashback <= 0) {
            abort(404);
        }
        $usd = Currency::first()->value;
        $current_amount = $current_cashback * $usd * 100;
        $response = $this->makePostToApelsin($card, $current_amount);
        $status_code = $response->getStatusCode();
        if ($status_code == 200) {
            User::where('id', $user->id)->update([
                'cashback' => ($user->cashback * 1) + ($current_cashback * 1)
            ]);
        }
        return response(['statusCode' => $status_code, 'cashback' => $current_cashback],
            $status_code);
    }

    public function makePostToApelsin($card, $amount)
    {
        $login = 'topup-fulibu';
        $password = 'PwsDF,S$?{`3~>D2';
        $client = new Client();
        $response = $client->post('https://topup.kapitalbank.uz/api/merchant', [
            'headers' => ['Content-type' => 'application/json'],
            'auth' => [$login, $password],
            'json' => [
                'method' => 'fulibu.cashback',
                'params' => [
                    'card' => $card,
                    'amount' => $amount, // send amount in tiyn's
                ]
            ]
        ]);

        return $response;
    }
}
