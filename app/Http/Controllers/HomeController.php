<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Collection;
use App\Order;
use App\User;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('main');
    }
    public function main(){
        return view('welcome');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders = Order::where('is_paid',true)->get();
        $users = User::where('id','!=',1)->get();
        $books = Book::withCount('orders')->orderBy('orders_count','DESC')->get();
        $collections = Collection::all();
        $authors = Author::all();
        return view('backend.index',compact('orders','users','collections','books','authors'));
    }
    public function refresh()
    {
//        $this->sendNotification('fNu5brGtlEyVhhfqOBtKj6:APA91bFH_A1geU5tJPrOGuHOgCyWVtmcy2J9evy2RMnFb1cPqWZSfdgoER1fDSbXMOYKaUubG7HISZC8wpAvY1yKhxrttaJZ4LUluZHup7VPD7JLKAUPbDWmicSimxNBR1vHCe6e8hEr','test','test');

        \Artisan::call('optimize');
//        \Artisan::call('optimize:clear');
        return redirect()->back();
    }
    public function clearCache(){
        \Artisan::call('cache:clear');
        return redirect()->back();
    }
    public function clearRoute(){
        \Artisan::call('route:clear');
        return redirect()->back();
    }
    public function clearView(){
        \Artisan::call('view:clear');
        return redirect()->back();
    }
    public function clearConfig(){
        \Artisan::call('config:cache');
        return redirect()->back();
    }
    public function clearOptimize(){
        \Artisan::call('optimize:clear');
        return redirect()->back();
    }
}
