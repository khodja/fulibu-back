<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use App\Book;
use App\Category;
use App\Type;
use App\Traits\ImageHandler;

class BookController extends Controller
{
    use ImageHandler;

    private $validation = [
        'name' => 'required',
        'description' => 'required',
        'size' => 'required',
        'author_id' => 'required',
        'rating' => 'required',
        'publish_date' => 'required',
    ];

    public function show($id)
    {
        $data = Book::with('author', 'categories', 'types')->findOrFail($id);
        return response()->json(['statusCode' => 200, 'data' => $data], 200);
    }

    public function getExtensions($id)
    {
        $data = Book::findOrFail($id)->getBookExtensions();
        return response()->json(['statusCode' => 200, 'data' => $data], 200);
    }

    public function getBook($id)
    {
        $book = Book::findOrFail($id);
        $checker = $book->getIsBoughtAttribute();
        if (!$checker) {
            return abort(404);
        }
        $extension = request('extension');
        $find_files = \File::glob(storage_path("app/books/" . $book->id . "/*." . $extension));
        if (count($find_files)) {
            return response()->download($find_files[0]);
        } else {
            return abort(404);
        }
    }

    public function index()
    {
        $data = Book::paginate(20);
        return view('backend.book.index', compact('data'));
    }

    public function create()
    {
        $categories = Category::all();
        $types = Type::all();
        $authors = Author::all();
        return view('backend.book.create', compact('categories', 'authors', 'types'));
    }

    public function edit($id)
    {
        $data = Book::where('id', $id)->with('types', 'categories')->firstOrFail();
        $categories = Category::all();
        $types = Type::all();
        $authors = Author::all();
        $directory = "uploads/book/" . $data->id;
        $images = \File::glob($directory . "/*.jpg");
        $books = \File::glob(storage_path("app/books/" . $data->id . "/*"));
        return view('backend.book.edit', compact('data', 'categories', 'authors', 'images', 'books', 'types'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);

        $book = Book::create($request->except('_token', 'image', 'books', 'category_id', 'type_id',
            'jfiler-items-exclude-image-0'));
        $book->categories()->attach($request->category_id);
        $book->types()->attach($request->type_id);
        $this->filesHandler($request, $book);
        return redirect()->action('BookController@index')->with('success', 'Успешно добавлено');
    }

    public function switch(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        $book->update([
            'recommend' => !$book->recommend
        ]);
        return redirect()->action('BookController@index')->with('success', 'Изменения успешно внесены');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        $book = Book::where('id', $id)->firstOrFail();
        $book->update($request->except('_token', 'image', 'books', 'category_id',
            '_method', 'type_id', 'jfiler-items-exclude-image-0'));
        if ($request->category_id) {
            $book->categories()->sync($request->category_id);
        }
        if ($request->type_id) {
            $book->types()->sync($request->type_id);
        }
        $this->filesHandler($request, $book);

        return redirect()->action('BookController@index')->with('success', 'Успешно изменено');
    }

    public function filesHandler($request, $book)
    {
        if ($request->file('image')) {
            $this->createFolder('book', $book->id);
            $images = $request->file('image');
            $this->storeImages($images, 'book', $book->id, 170, 230);
        }
        if ($request->file('books')) {
            $this->createBookFolder('books', $book->id);
            $books = $request->file('books');
            $this->storeBooks($books, 'books', $book->id);
        }
    }

    public function removeBook($id)
    {
        \request()->validate([
            'file_name' => 'required'
        ]);
        $status = $this->removeBooks('books', $id, request('file_name'));
        if ($status) {
            return ['statusCode' => 200];
        }
        return abort(404);
    }

    public function delete($id)
    {
        $book = Book::findOrFail($id);
        $book->collection()->detach();
        $book->categories()->detach();
        $book->types()->detach();
        $book->orders()->delete();
        $book->delete();
        return redirect()->action('BookController@index')->with('success', 'Успешно');
    }

    public function removeImage($id)
    {
        \request()->validate([
            'file_name' => 'required'
        ]);
        $status = $this->removeImages('book', $id, request('file_name'));
        if ($status) {
            return ['statusCode' => 200];
        }
        return abort(404);
    }
}
