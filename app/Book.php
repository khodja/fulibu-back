<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Book extends Model
{
    protected $guarded = [];
    protected $appends = ['image', 'is_bought'];

    public function collection()
    {
        return $this->belongsToMany('App\Collection', 'books_collection');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_book');
    }

    public function types()
    {
        return $this->belongsToMany('App\Type', 'types_book');
    }

    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'item_id', 'id');
    }

    public function getImageAttribute()
    {
        $directory = "uploads/book/" . $this->id;
        $images = \File::glob($directory . "/*.jpg");
        if (count($images) > 0) {
            $result = [];
            foreach ($images as $image) {
                array_push($result, asset($image));
            }
            return $result;
        }
        return [asset('img/no-photo.png')];
    }

    public function getIsBoughtAttribute()
    {
        $user = auth('api')->user();
        if ($user) {
            $checker = $user->orders()->where([['item_id','=', $this->id], ['is_paid','=', true]])->first();
            if ($checker) {
                return true;
            }
        }
        return false;
    }

    public function getBookExtensions()
    {
        $user = auth('api')->user();
        if ($user) {
            $is_bought = $this->getIsBoughtAttribute();
            if ($is_bought) {
                $books = \File::glob(storage_path("app/books/" . $this->id . "/*"));
                if (count($books)) {
                    $result = [];
                    foreach ($books as $book) {
                        array_push($result, \File::extension($book));
                    }
                    return $result;
                }
            }
        }
        return [];
    }
}
