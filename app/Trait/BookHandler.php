<?php

namespace App\Traits;

trait BookHandler
{
    public function bookHandle($request, $folderName, $item)
    {
        if ($request->file('books')) {
            $this->createBookFolder($folderName, $item->id);
            $files = $request->file('books');
            $this->storeBooks($files, $item->id);
        }
    }

    public function createBookFolder($directory, $itemId)
    {
        $directory_path = public_path('books/' . $directory);
        $path = public_path('books/' . $directory . '/' . $itemId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeBooks($files, $folderName, $id)
    {
        foreach ($files as $key => $file) {

            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            $path[] = $file->storeAs('books/' . $folderName . '/' . $id, $fileNameToStore);
        }
        return 1;
    }

    public function removeBooks($folderName, $id, $fileName)
    {
        $pathToDestroy = public_path('books/' . $folderName . '/' . $id . '/' . $fileName);
        \File::delete($pathToDestroy);
        return 1;
    }
}
