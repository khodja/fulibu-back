<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\File;

trait ImageHandler
{
    public function imageHandle($request, $folderName, $item)
    {
        if ($request->file('image')) {
            $this->createFolder($folderName, $item->id);
            $images = $request->file('image');
            $this->storeImages($images, $item->id);
        }
    }

    public function createFolder($directory, $itemId)
    {
        $directory_path = public_path('uploads/'.$directory);
        $path = public_path('uploads/'.$directory.'/'.$itemId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $folderName, $id, $width, $height , $format = null)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now").($format ? '.'.$format : '.jpg');
            $path = public_path('uploads/'.$folderName.'/'.$id.'/'.$filename);
            Image::make($image->getRealPath())->encode($format ? $format : 'jpg', 100)
                ->fit($width, $height)
                ->save($path);
        }
        return 1;
    }

    public function removeImages($folderName, $id, $fileName)
    {
        $pathToDestroy = public_path('uploads/'.$folderName.'/'.$id.'/'.$fileName);
        \File::delete($pathToDestroy);
        return 1;
    }

    public function bookHandle($request, $folderName, $item)
    {
        if ($request->file('books')) {
            $files = $request->file('books');
            $this->storeBooks($files, $folderName, $item->id);
        }
    }

    public function createBookFolder($directory, $itemId)
    {
        $directory_path = public_path('public/storage/books/'.$directory);
        $path = public_path('public/storage/books/'.$directory.'/'.$itemId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeBooks($files, $folderName, $id)
    {
        foreach ($files as $key => $file) {
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            Storage::disk('local')->putFileAs($folderName.'/'.$id, $file, $fileNameToStore);
        }
        return 1;
    }

    public function removeBooks($folderName, $id, $fileName)
    {
//        $pathToDestroy = storage_path($folderName.'/'.$id.'/'.$fileName);
//        dd($pathToDestroy);
//        \File::delete($pathToDestroy);
        Storage::disk('local')->delete($folderName.'/'.$id.'/'.$fileName);
        return 1;
    }
}
