<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guarded = [];

    protected $appends = ['friends'];

    protected $hidden = [
        'password', 'remember_token', 'email'
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public $reward = [0, 22, 50, 100, 200, 300];

    public function children()
    {
        return $this->hasMany(self::class, 'user_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function hasRole($role)
    {
        return $this->roles()->where('type', $role)->first();
    }

    public function getCashback()
    {
        return $this->cashback;
    }

    public function hasPayedOrder()
    {
        $getOrder = $this->orders()->where('is_paid', true)->count();
        if ($getOrder > 0) {
            return true;
        }
        return false;
    }

    public function getUserLevelCashBack($orders_count)
    {
        if ($orders_count >= 0 && $orders_count < 4) return [0, 0, 4];
        if ($orders_count >= 4 && $orders_count < 20) return [1, 22, 20];
        if ($orders_count >= 16 && $orders_count < 84) return [2, 50, 84];
        if ($orders_count >= 64 && $orders_count < 340) return [3, 100, 340];
        if ($orders_count >= 256 && $orders_count < 1364) return [4, 200, 1364];
        if ($orders_count >= 1024 && $orders_count < 5460) return [5, 300, 5460];
        return [6, 'spark', 99999];
    }

    public function getFriendsAttribute()
    {
        $getFriends = $this->getAllChildren();
        $friends_count = count($getFriends);
        $order_sum = 0;
        $order_sum_every = [];
        foreach ($getFriends as $ele) {
            $sum = $ele->orders->sum('price');
            if ($sum > 0) {
                $order_sum += $sum;
                array_push($order_sum_every, $sum);
            }
        }
        if (!$this->hasPayedOrder()) {
            $friends_count = 0;
            $order_sum_every = [];
        }
        $levelCash = $this->getUserLevelCashBack($friends_count);
        $order_count = count($order_sum_every);
        $level = $levelCash[0];
        $next_level = $levelCash[2];
        $reward = 0;
        $rewardArray = $this->reward;
        foreach ($rewardArray as $key => $rew) {
            if ($key <= $level) {
                if (is_numeric($rew)) {
                    $reward = $reward + $rew;
                }
            }
        }
        $current_cashback = $this->getCashback();
        $reward = $reward - $current_cashback;
        return [
            'friends_count' => $friends_count,
            'next_level' => $next_level,
            'level' => $level,
            'order_count' => $order_count,
            'order_sum' => $order_sum,
            'reward' => $reward
        ];
    }

    public function library()
    {
        return $this->hasMany('App\Library');
    }

    public function getAllChildren()
    {
        $data = collect();
        $children = $this->children;

        while ($children->count()) {
            $child = $children->shift();
            if ($child->orders()->where('is_paid', true)->sum('price') >= 22) {
                $data->push($child);
                if ($child->children->count() >= 4) {
                    $children = $children->merge($child->children);
                }
            }
        }
        return $data;
    }
}
