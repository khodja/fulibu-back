<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function books()
    {
        return $this->belongsToMany('App\Book', 'books_collection')->withTimestamps();
    }
}
