@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('BookController@store') }}" class="form" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{old('name')}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description">Описание</label>
                                    <input required="required" type="text" name="description"
                                           value="{{old('description')}}" class="form-control regStepOne"
                                           id="description" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="size">Обьем (кол-во страниц)</label>
                                    <input required="required" type="number" min="1" name="size"
                                           value="{{old('size')}}" class="form-control regStepOne"
                                           id="size" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="rating">Рейтинг</label>
                                    <input required="required" type="number" min="1" name="rating"
                                           value="{{old('rating')}}" class="form-control regStepOne"
                                           id="rating" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена ($ - USD)</label>
                                    <input required="required" step="0.1" type="number" min="1" name="price"
                                           value="{{old('price')}}" class="form-control regStepOne"
                                           id="price" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="author">Выберите автора</label>
                                    <select name="author_id" required id="author"
                                            class="chosen-select form-control">
                                        @foreach( $authors as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите тип</label>
                                    <select multiple name="type_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $types as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select multiple name="category_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $categories as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="publish_date">Дата публикации</label>
                                    <input required="required" type="date" name="publish_date"
                                           value="{{old('publish_date')}}"
                                           class="form-control regStepOne" id="publish_date" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте общие фотографии. Мин.
                                размер изображений 310x420 пикс.</p>
                            <div class="fotoUploader">
                                <input required="required" type="file" name="image[]" class="filer_input2"
                                       multiple="multiple">
                            </div>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте файлы Книги</p>
                            <div class="fotoUploader">
                                <input type="file" multiple name="books[]">
                            </div>
                            <br><br>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(function () {
            $('.chosen-select').chosen();
        });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
