@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('BookController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data"
                      class="form"
                >
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{$data->name}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description">Описание</label>
                                    <input required="required" type="text" name="description"
                                           value="{{$data->description}}" class="form-control regStepOne"
                                           id="description" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="size">Обьем (кол-во страниц)</label>
                                    <input required="required" type="number" min="1" name="size"
                                           value="{{$data->size}}" class="form-control regStepOne"
                                           id="size" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="rating">Рейтинг</label>
                                    <input required="required" type="number" min="1" name="rating"
                                           value="{{$data->rating}}" class="form-control regStepOne"
                                           id="rating" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена ($ - USD)</label>
                                    <input required="required" step="0.1" type="number" min="1" name="price"
                                           value="{{$data->price}}" class="form-control regStepOne"
                                           id="price" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="author">Выберите автора</label>
                                    <select name="author_id" required id="author"
                                            class="chosen-select form-control">
                                        @foreach( $authors as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if($datas->id == $data->author_id) selected @endif>{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите тип</label>
                                    <select multiple name="type_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $types as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if( in_array($datas->id, $data->types->pluck('id')->toArray())) selected @endif>{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select multiple name="category_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $categories as $datas )
                                            <option
                                                value="{{ $datas->id }}"
                                                @if( in_array($datas->id, $data->categories->pluck('id')->toArray())) selected @endif>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="publish_date">Дата публикации</label>
                                    <input required="required" type="date" name="publish_date"
                                           value="{{$data->publish_date}}"
                                           class="form-control regStepOne" id="publish_date" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте общие фотографии. Мин.
                                размер изображений 310x420 пикс.</p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($images as $image)
                                        <li class="gallryUploadBlock_item photo-thumbler d-inline-block"
                                            data-image="{{basename($image)}}">
                                            <div class="jFiler-item-thumb-image">
                                                <img src="{{asset($image)}}" alt="image">
                                            </div>
                                            <div class="removeItem" data-image="{{basename($image)}}">
                                                <span class="deletePhoto" data-image="{{basename($image)}}">
                                                    <i class="fe fe-minus"></i>
                                                </span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <input type="file" name="image[]" class="filer_input3" multiple="multiple">
                            </div>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавленные книги , если хотите загрузите ещё</p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($books as $book)
                                        <li class="gallryUploadBlock_item photo-thumbler d-inline-block"
                                            data-item="{{basename($book)}}">
                                            <div class="jFiler-item-thumb-image">
                                                <img src="{{asset('img/no-photo.png')}}" alt="image">
                                            </div>
                                            <div class="removeItem" data-item="{{basename($book)}}">
                                                <span class="deleteItem" style="cursor: pointer;"
                                                      data-item="{{basename($book)}}">
                                                    <i class="fe fe-minus"></i>
                                                </span>
                                            </div>
                                            <p>{{basename($book)}}</p>
                                        </li>
                                    @endforeach
                                </ul>
                                <input type="file" multiple name="books[]">
                            </div>
                            <br><br>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".filer_input3").filer({
                limit: null,
                maxSize: null,
                extensions: null,
                changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
                showThumbs: true,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
                    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
                    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
                    </li>`,
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: true,
                    canvasImage: true,
                    removeConfirmation: false,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.fe.fe-minus'
                    }
                },
                dragDrop: {
                    dragEnter: null,
                    dragLeave: null,
                    drop: null,
                    dragContainer: null,
                },
                files: null,
                addMore: true,
                allowDuplicates: false,
                clipBoardPaste: true,
                excludeName: null,
                beforeRender: null,
                afterRender: null,
                beforeShow: null,
                beforeSelect: null,
                itemAppendToEnd: true,
                onSelect: null,
                afterShow: function (jqEl, htmlEl, parentEl, itemEl) {
                    $('.galleryAddElement').hide()
                    $('.cloneGalleryAddElement').remove()
                    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
                },
                onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                    let filerKit = inputEl.prop("jFiler"),
                        file_name = filerKit.files_list[id].name;
                    //   $.post('./php/ajax_remove_file.php', {file: file_name});
                    if (filerKit.files_list.length == 1) {
                        $('.galleryAddElement').show()
                    }
                },
                onEmpty: null,
                options: null,
            });
            $('.uploader').on('click', '.cloneGalleryAddElement', function (e) {
                $('.galleryAddElement').trigger('click');
            });
            $('.deletePhoto').click((e) => {
                let removeImage = $(e.target);
                let imageData = removeImage.data('image');
                if (confirm('Вы уверены?')) {
                    let deleteItem = $.get("{{action('BookController@removeImage',$data->id)}}", {file_name: imageData});
                    deleteItem.done(() => {
                        removeImage.parent().parent().remove();
                    });
                }
            });
            $('.deleteItem').click((e) => {
                let removeImage = $(e.target);
                let fileName = removeImage.data('item');
                if (confirm('Вы уверены?')) {
                    let deleteItem = $.get("{{action('BookController@removeBook',$data->id)}}", {file_name: fileName});
                    deleteItem.done(() => {
                        removeImage.parent().parent().remove();
                    });
                }
            });
            $('.chosen-select').chosen();
            $('.chosen-select-deselect').chosen({allow_single_deselect: true});
        });
        $(function () {
            $('#category').chosen();
        });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
