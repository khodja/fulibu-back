@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Книги</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block">
                        <ul class="list-block">
                            @foreach ($data as $item)
                                <li class="item">
                                    <div class="left-block d-flex justify-content-between align-items-center"
                                         style="min-height: 50px">
                                <span>
                                    <i class="fe fe-list"></i> {{ $item->name }}
                                </span>
                                    </div>
                                    <div class="right-block">
                                        <a href="{{ action('BookController@edit',$item->id) }}"
                                           class="btn btn-outline-primary mr-5">Изменить</a>
                                        <span class="icon">
										<label class="custom-switch">
										  <input type="checkbox" name="custom-switch-checkbox"
                                                 class="custom-switch-input" @if($item->recommend) checked="checked" @endif>
										  <span class="custom-switch-indicator actioner"
                                                data-href="{{action('BookController@switch',$item->id)}}"></span>
										  <span class="custom-switch-description"></span>
										</label>
                                        <form class="d-inline-block"
                                              action="{{ action('BookController@delete' , $item->id) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn icon border-0"
                                                    onclick="return confirm('Вы уверены?')">
                                                <i class="fe fe-trash"></i>
                                            </button>
                                        </form>
									</span>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('BookController@create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                        <div class="d-flex align-items-center justify-content-end">
                            {{ $data->links() }}
                        </div>
                    </div>
                    <div class="button-block">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('.actioner').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
