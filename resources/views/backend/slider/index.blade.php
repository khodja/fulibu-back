@extends('layouts.backend')

@section('content')
<section>
    <div class="container">
        <h2 class="blue-title">Баннеры</h2>
        <div class="row">
                <form class="form" action="#" autocomplete="off" class="form">
                <div class="white-block hotels">
                    <ul class="list-block">
                        @foreach($data as $datas)
                        <li class="item">
                            <a class="left-block" href="{{ action('SliderController@edit' , $datas->id) }}">
                                <p class="pl-2">{{ $datas->name_ru  }}</p>
                            </a>
                            <div class="right-block">
                                <a href="{{ action('SliderController@edit' , $datas->id) }}" class="btn btn-primary">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </div>
                        </li>
                        @endforeach
                        <a class="add-list-btn" href="{{ action('SliderController@create') }}">
                            <i class="fe fe-plus-circle"></i>
                            Добавить
                        </a>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
