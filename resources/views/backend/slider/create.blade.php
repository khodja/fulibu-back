@extends('layouts.backend')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <form action="{{ action('SliderController@store') }}" method="POST" enctype="multipart/form-data" class="form">
                @csrf
                <div class="white-block mb-30">
                  <div class="head">
                      <h3>Добавить Баннер</h3>
                  </div>
                  <div class="content">
                    <div class="input-block">
                        <div class="input">
                            <label for="name_ru">Название (RU)</label>
                            <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}" class="form-control regStepOne" id="name_ru" />
                        </div>
                    </div>
                    <div class="input-block">
                        <div class="input">
                            <label for="name_uz">Название (UZ)</label>
                            <input required="required" type="text" name="name_uz" value="{{old('name_uz')}}" class="form-control regStepOne" id="name_ru" />
                        </div>
                    </div>
                    <div class="input-block">
                        <div class="input">
                            <label for="name_uz">Название (EN)</label>
                            <input required="required" type="text" name="name_en" value="{{old('name_en')}}" class="form-control regStepOne" id="name_ru" />
                        </div>
                    </div>
                  <div class="input-block">
                    <div class="input">
                        <label>Книга:</label>
                        <select name="vendor_id" class="form-control custom-select">
                            <option value="0">Не выбран</option>
                            @foreach( $vendor as $datas )
                                <option value="{{ $datas->id }}">{{ $datas->name }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="input-block">
                    <div class="input">
                        <label>Фото 680x540:</label>
                        <input type="file" required="required" name="image[]">
                    </div>
                  </div>
                  </div>
                </div>
                <div class="button-block">
                    <button type="submit" class="continue-btn">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
