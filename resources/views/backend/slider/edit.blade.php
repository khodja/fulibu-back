@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('SliderController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" class="form">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{$data->name_ru}}"
                                           class="form-control regStepOne" id="name_ru"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{$data->name_uz}}"
                                           class="form-control regStepOne" id="name_uz"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_en">Название (EN)</label>
                                    <input required="required" type="text" name="name_en" value="{{$data->name_en}}"
                                           class="form-control regStepOne" id="name_en"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Книга:</label>
                                    <select name="book_id" class="form-control custom-select">
                                        <option value="0">Не выбран</option>
                                        @foreach( $vendor as $datas )
                                            <option value="{{ $datas->id }}"
                                                    @if($data->book_id == $datas->id) selected @endif>{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Фото:</label>
                                    <input type="file" name="image[]" class="image">
                                    <br>
                                    <h4>Загрузите для обновления фотографии 680x540</h4>
                                    <br>
                                    <div class="d-flex justify-content-between mt-5">
                                        @foreach($images as $image)
                                            <img src="{{asset($image)}}?time={{microtime(true)}}" alt="image"
                                                 width="150"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
