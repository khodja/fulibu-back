@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>
                            Статистика
                        </div>
                        <div>
                            <a href="{{action('HomeController@refresh')}}" class="btn btn-warning">Сбросить кэш
                                сайта</a>
{{--                            <a href="{{action('HomeController@clearCache')}}" class="btn btn-warning">Clear cache</a>--}}
                            <a href="{{action('HomeController@clearRoute')}}" class="btn btn-warning">ClearRoute</a>
                            <a href="{{action('HomeController@clearView')}}" class="btn btn-warning">Clear View</a>
                            <a href="{{action('HomeController@clearConfig')}}" class="btn btn-warning">Clear Config</a>
                            <a href="{{action('HomeController@clearOptimize')}}" class="btn btn-warning">Clear Optimize</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$orders->count()}}</div>
                                <h3 class="mb-1">Кол-во заказов</h3>
                                <div class="text-muted">Закрытых</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$books->count()}}</div>
                                <h3 class="mb-1">Кол-во книг</h3>
                                <div class="text-muted">Загруженных</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$collections->count()}}</div>
                                <h3 class="mb-1">Кол-во коллекций</h3>
                                <div class="text-muted">Загруженных</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$users->count()}}</div>
                                <h3 class="mb-1">Кол-во юзеров</h3>
                                <div class="text-muted">Активных</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h2 class="card-title">Самые продаваемые книги</h2>
                            </div>
                            <table class="table card-table">
                                <tbody>
                                @foreach($books as $key => $book)
                                    <tr>
                                        <td>{{$book->name}}</td>
                                        <td class="text-right">
                                            <span class="badge badge-default">{{$book->orders_count}} шт</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body p-3 text-center">
                                <div class="text-right text-green">
                                    {{$orders->where('created_at', \Carbon\Carbon::today())->count()}} шт
                                    <i class="fe fe-chevron-up"></i>
                                </div>
                                <div class="h1 m-0">{{$orders->where('created_at', \Carbon\Carbon::today())->sum('price')}} $</div>
                                <div class="text-muted mb-4">Продано сегодня</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body p-3 text-center">
                                <div class="text-right text-green">
                                    {{$orders->count()}} шт
                                    <i class="fe fe-chevron-up"></i>
                                </div>
                                <div class="h1 m-0">{{$orders->sum('price')}} $</div>
                                <div class="text-muted mb-4">Продано всего</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
