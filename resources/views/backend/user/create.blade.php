@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('UserController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">ФИО</label>
                                    <input required="required" name="name" type="text" class="form-control"
                                           id="name" placeholder="Введите название"/>
                                </div>
                            </div>
                            <input type="hidden" name="is_business" value="0">
                            <div class="input-block">
                                <div class="input">
                                    <label for="birthday">День рождения:</label>
                                    <input required="required" name="birthday" type="date" class="form-control"
                                           id="birthday" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="phone">Телефон</label>
                                    <input required="required" name="phone" type="tel" class="form-control"
                                           id="phone" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="email">E-mail</label>
                                    <input required="required" name="email" type="text" class="form-control"
                                           id="email" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="sex">Пол:</label>
                                    <select name="sex" id="sex" class="form-control">
                                        <option value="male">Мужчина</option>
                                        <option value="female">Женщина</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="country">Регион:</label>
                                    <select name="country" id="country" class="form-control">
                                        @foreach($regions as $region)
                                            <option value="male">{{$region->name_ru}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте фотографию пользователя.</p>
                            <div class="uploader">
                                <input type="file" name="image">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

