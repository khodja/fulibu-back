@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Пользователи</h3>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th class="text-center">Имя</th>
                        <th class="text-center">Телефон</th>
                        <th class="text-center">друзья | покупки друзей | сумма покупок друзей</th>
                        <th class="text-center">Уровень | Кэшбек</th>
                        <th class="text-center">Потратил сам</th>
                        <th class="text-center">Дествия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $datas)
                        @if($datas->id !== 1)
                            <tr>
                                <td class="text-center">{{ $datas->name }}</td>
                                <td class="text-center">{{ $datas->phone }}</td>
                                @php
                                    $friends = $datas->friends;
                                @endphp
                                <td class="text-center">
                                    <div class="badge badge-secondary">{{$friends['friends_count']}}</div>
                                    <div class="badge badge-primary">{{ $friends['order_count']  }} шт</div>
                                    <div class="badge badge-success"> {{ $friends['order_sum']  }} $</div>
                                </td>
                                <td class="text-center">
                                    <div
                                        class="badge badge-primary">
                                        {{$friends['level']}}
                                    </div>
                                    <div
                                        class="badge badge-success">{{$friends['reward']}}
                                        $
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div
                                        class="badge badge-success">{{$datas->orders->where('is_paid',true)->sum('price')}}
                                        $
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a href="{{action('UserController@links',$datas->id)}}" class="btn btn-primary">Друзья
                                        по линку</a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-end">
            {{ $data->links() }}
        </div>
    </div>
@endsection
