@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('UserController@update',$data->alias) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off" class="form">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{ $data->name_ru }}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{ $data->name_uz }}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_kr">Название (KR)</label>
                                    <input required="required" type="text" name="name_kr" value="{{ $data->name_kr }}"
                                           class="form-control regStepOne" id="name_kr" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{ $data->description_ru }}" class="form-control regStepOne"
                                           id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{ $data->description_uz }}" class="form-control regStepOne"
                                           id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_kr">Описание (KR)</label>
                                    <input required="required" type="text" name="description_kr"
                                           value="{{ $data->description_kr }}" class="form-control regStepOne"
                                           id="description_kr" placeholder=""/>
                                </div>
                            </div>
                            @if($data->parent_id != null)
                                <div class="input-block">
                                    <div class="input">
                                        <label>Привязанная категория:</label>
                                        <select name="parent_id" class="form-control custom-select">
                                            <option value="0">Отвязать от всех</option>
                                            @foreach( $categories as $datas )
                                                <option value="{{ $datas->id }}"
                                                        @if($data->parent_id == $datas->id) selected @endif>{{ $datas->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="input-block">
                                <div class="input">
                                    <label for="color">Выберите цвет: </label> <br>
                                    <input required="required" value="{{$data->color}}" class="jscolor" id="color"
                                           name="color" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Фотография</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте фотографию для изменения. Размер изображений 120x170 пикс</p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    <img src="{{asset($image)}}?t='{{microtime(true)}}" alt="image" width="200">
                                </ul>
                                <input type="file" name="image" required>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{asset('backend/js/vendors/jscolor.js')}}"></script>
@endsection
