@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('CollectionController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{old('name')}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description">Описание</label>
                                    <input required="required" type="text" name="description"
                                           value="{{old('description')}}" class="form-control regStepOne"
                                           id="description" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="rating">Рейтинг</label>
                                    <input required="required" type="number" min="1" name="rating"
                                           max="5"
                                           value="{{old('rating')}}" class="form-control regStepOne"
                                           id="rating" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите книгу</label>
                                    <select multiple name="book_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $books as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="publish_date">Дата публикации</label>
                                    <input required="required" type="date" name="publish_date" value="{{old('publish_date')}}"
                                           class="form-control regStepOne" id="publish_date" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(function () {
            $('#category').chosen();
        });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
