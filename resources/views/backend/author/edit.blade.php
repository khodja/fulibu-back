@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('AuthorController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off" class="form">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Имя</label>
                                    <input required="required" type="text" name="name" value="{{ $data->name }}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
