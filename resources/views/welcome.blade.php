<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fulibu System</title>
</head>
<style>
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    body{
        background: #3A3F4E;
        text-align: center;
        height: 100vh;
        width: 100vw;
        overflow: hidden;
    }
    img{
        height: 100vh;
        min-height: 400px;
        object-fit: contain;
    }
</style>
<body>
<a href="/login">
    <img src="{{asset('backend/images/main-back.svg')}}" alt="/">
</a>
</body>
</html>
