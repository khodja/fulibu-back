<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('type', 'admin')->first();

        $admin = new User;
        $admin->name = 'admin';
        $admin->password = bcrypt('fulibu2020');
        $admin->phone = '999999999';
        $admin->email = 'admin@gmail.com';
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
