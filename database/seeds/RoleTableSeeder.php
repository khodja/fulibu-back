<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'type' => 'admin'
        ]);
        Role::create([
            'type' => 'manager'
        ]);
        Role::create([
            'type' => 'member'
        ]);
    }
}
