<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@main');
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('dashboard', 'HomeController@index')->name('home');
    Route::get('dashboard/refresh', 'HomeController@refresh');
    Route::get('dashboard/clearCache', 'HomeController@clearCache');
    Route::get('dashboard/clearRoute', 'HomeController@clearRoute');
    Route::get('dashboard/clearView', 'HomeController@clearView');
    Route::get('dashboard/clearConfig', 'HomeController@clearConfig');
    Route::get('dashboard/clearOptimize', 'HomeController@clearOptimize');

    Route::get('dashboard/category', 'CategoryController@index');
    Route::get('dashboard/category/show/{id}', 'CategoryController@show');
    Route::get('dashboard/category/create', 'CategoryController@create');
    Route::get('dashboard/category/edit/{id}', 'CategoryController@edit');
    Route::post('dashboard/category/store', 'CategoryController@store');
    Route::put('dashboard/category/update/{id}', 'CategoryController@update');
    Route::delete('dashboard/category/delete/{id}', 'CategoryController@delete');
//
    Route::get('dashboard/author', 'AuthorController@index');
    Route::get('dashboard/author/show/{id}', 'AuthorController@show');
    Route::get('dashboard/author/create', 'AuthorController@create');
    Route::get('dashboard/author/edit/{id}', 'AuthorController@edit');
    Route::post('dashboard/author/store', 'AuthorController@store');
    Route::put('dashboard/author/update/{id}', 'AuthorController@update');
//
    Route::get('dashboard/type', 'TypeController@index');
    Route::get('dashboard/type/show/{id}', 'TypeController@show');
    Route::get('dashboard/type/create', 'TypeController@create');
    Route::get('dashboard/type/edit/{id}', 'TypeController@edit');
    Route::post('dashboard/type/store', 'TypeController@store');
    Route::put('dashboard/type/update/{id}', 'TypeController@update');
//
    Route::get('dashboard/user', 'UserController@index');
    Route::get('dashboard/user/{id}/links', 'UserController@links');
    Route::get('dashboard/user/show/{id}', 'UserController@show');
    Route::get('dashboard/user/create', 'UserController@create');
    Route::get('dashboard/user/edit/{id}', 'UserController@edit');
    Route::post('dashboard/user/store', 'UserController@store');
    Route::put('dashboard/user/update/{id}', 'UserController@update');
//
    Route::get('dashboard/books', 'BookController@index');
    Route::get('dashboard/books/removeImage/{id}', 'BookController@removeImage');
    Route::get('dashboard/books/removeBook/{id}', 'BookController@removeBook');
    Route::get('dashboard/books/create', 'BookController@create');
    Route::get('dashboard/books/edit/{id}', 'BookController@edit');
    Route::post('dashboard/books/store', 'BookController@store');
    Route::put('dashboard/books/update/{id}', 'BookController@update');
    Route::get('dashboard/books/switch/{id}', 'BookController@switch');
    Route::delete('dashboard/books/delete/{id}', 'BookController@delete');


    Route::get('dashboard/collections', 'CollectionController@index');
    Route::get('dashboard/collections/removeImage/{id}', 'CollectionController@removeImage');
    Route::get('dashboard/collections/create', 'CollectionController@create');
    Route::get('dashboard/collections/edit/{id}', 'CollectionController@edit');
    Route::post('dashboard/collections/store', 'CollectionController@store');
    Route::put('dashboard/collections/update/{id}', 'CollectionController@update');

    Route::get('/dashboard/slider', 'SliderController@index');
    Route::get('/dashboard/slider/create', 'SliderController@create');
    Route::get('/dashboard/slider/edit/{id}', 'SliderController@edit');
    Route::post('/dashboard/slider/store', 'SliderController@store');
    Route::put('/dashboard/slider/update/{id}', 'SliderController@update');
    Route::delete('/dashboard/slider/delete/{id}', 'SliderController@delete');


    Route::get('dashboard/currency', 'CurrencyController@get');
    Route::get('dashboard/currency/update', 'CurrencyController@update');
//
});

