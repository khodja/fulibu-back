<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('verification', 'AuthController@verification');
Route::get('slider', 'SliderController@get');
Route::get('categories', 'CategoryController@get');
Route::post('auth', 'AuthController@auth');
Route::get('main', 'CollectionController@get');
Route::get('collection/{id}', 'CollectionController@show');
Route::get('book/{id}', 'BookController@show');
Route::get('search', 'CollectionController@search');
Route::get('currency', 'CurrencyController@getCurrency');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('user', 'AuthController@getUser');
    Route::get('orders', 'OrderController@get');
    Route::get('extension/book/{id}', 'BookController@getExtensions');
    Route::put('account', 'AuthController@register');
    Route::get('logout', 'AuthController@logout');
    Route::get('library', 'LibraryController@get');
    Route::post('checkout', 'OrderController@checkout');
    Route::post('cashback', 'OrderController@cashback');
    Route::get('get/book/{id}', 'BookController@getBook');
    Route::get('toggle/library/{id}', 'LibraryController@toggle');
});
Route::post('handle/checkout', 'OrderController@handleCheckout');
